<?php

namespace FAE\cachefactory;

use Symfony\Component\Cache\Adapter\ChainAdapter;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\Adapter\ApcuAdapter;

class factory
{

  const EXPIRES_ONE_MINUTE = 60;
  const EXPIRES_FIVE_MINUTES = 300;
  const EXPIRES_TEN_MINUTES = 600;
  const EXPIRES_ONE_HOUR = 3600;
  const EXPIRES_DAY = 86400;

  static public function getDefaultAdapter(string $namespace = '', int $lifetime = 0): AdapterInterface
  {
    global $config;

    $chain = [];

    // In container memory adapter (APCU)
    $chain[] = new ApcuAdapter($namespace, $lifetime);

    // Redis adapter
    if ( !empty($config->redis) && !empty($config->redis->enabled) ) {
      $chain[] = new RedisAdapter(new \Predis\Client($config->redis->url . ($config->redis->pw ? '?password=' . $config->redis->pw : '')), $namespace, $lifetime);
    }


    return new ChainAdapter($chain, $lifetime);
  }
  
}