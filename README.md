# Cache Factory 

A reusable wrapper for persistent caches for fae microservices within the ARIA platform ecosystem. 

This allows new containers to be spun up in dynamically scaling cluster context without them existing with an entirely empty cache.


## Usage

In your ENV

```

REDIS_URL=tcp://whatever:6379
REDIS_PW=blah
REDIS_ENABLED=true

```

Then in `app.php` configure `$config` with

```
'redis' => [
    'url' => configLoadEnv('REDIS_URL', 'tcp://redis-docker:6379'),
    'pw' => configLoadEnv('REDIS_PW', ''),
    'enabled' => configLoadEnv('REDIS_ENABLED', false),
]

```

Then in your code:

```
use FAE\cachefactory\factory;

$cache = factory::getDefaultAdapter('fae', factory::EXPIRES_ONE_HOUR);

```